# Snapcraft Multiarch Gitlab-CI

This repo contains a build script for GitLab CI. You may use this setup by adding the following to your `.gitlab-ci.yml` config:

```yaml
include:
  - project: lucyllewy/snapcraft-multiarch-gitlab-ci
    ref: main
    file: /snapcraft.yml

amd64:
  extends: [ .snapcraft, .amd64 ]
i386:
  extends: [ .snapcraft, .i386 ]
arm64:
  extends: [ .snapcraft, .arm64 ]
armhf:
  extends: [ .snapcraft, .armhf ]
ppc64el:
  extends: [ .snapcraft, .ppc64el ]
```

For self-hosted instances of GitLab and want to build snaps within the
CI, you may want to use this one instead:

```yaml
# ref: https://docs.gitlab.com/ee/ci/yaml/#includeremote
include:
  - remote: 'https://gitlab.com/lucyllewy/snapcraft-multiarch-gitlab-ci/-/raw/main/snapcraft.yml'

amd64:
  extends: [ .snapcraft, .amd64 ]
i386:
  extends: [ .snapcraft, .i386 ]
arm64:
  extends: [ .snapcraft, .arm64 ]
armhf:
  extends: [ .snapcraft, .armhf ]
ppc64el:
  extends: [ .snapcraft, .ppc64el ]
```

## Configuration for Build

You may customise the build with the following [variables](https://docs.gitlab.com/ee/ci/variables/):

| Variable | Description |
|-|-|
|SNAPCRAFT_PROJECT_ROOT|The directory containing your Snapcraft project. This is the directory containing either `snap/snapcraft.yaml`, `snapcraft.yaml` (without a `snap` directory at all), or `.snapcraft.yaml`|
|SNAPCRAFT_BUILD_INFO|Wether to include metadata about how the build was performed into the final snap. Proprietary application vendors might prefer this is disabled. May be either `1` to enable or `0` to disable. Default is enabled.|
|USE_SNAPCRAFT_CHANNEL|Override the snapcraft channel to install a non-default version of `snapcraft` to build with.|
|SNAPCRAFT_PARAMETERS|Extra parameters to pass on the `snapcraft` command line.|
|ARCHITECTURE|The architecture to build. This is usually set with the `extends` mechanism with the `.amd64`, `.i386`, etc templates|

## Publishing into Snap Store

Add a "file" variable to your GitLab repository named `STORE_LOGIN`.
This should contain the contents of the `exported.txt` file generated
when you run:

```bash
snapcraft export-login --snaps=$PACKAGE_NAME \
  --acls package_access,package_push,package_update,package_release \
  exported.txt
```

NOTE: Replace `$PACKAGE_NAME` in the command above with the name of
your snap.

With the variable in place, extend your Snapcraft build to include the
following snippet to automatically push into Snap Store in your
`.gitlab-ci.yml` file:

```yaml
publish-snap:
  extends: [ .publish-snap ]

  variables:
    # Can be any supported channel (stable,candidate, beta, edge)
    # or you can include tracks granted by the store admins
    # e.g. preview/edge, preview/stable, or 1.15/edge - naming scheme
    # depends on prior agreement/approval with the store admins via
    # forum.snapcraft.io - request your track names by posting in the
    # `store-requests` topic on the forum.
    RELEASE_CHANNEL: edge

  only:
    branches:
      # edit this if your main branch name is different
      - main
```
